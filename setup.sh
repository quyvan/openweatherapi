#!/bin/bash

if [ $# == 0 ]; then
  ENV="local"
elif [ $1 == "local" ]; then
  ENV="local"
elif [ $1 == "dev" ]; then
  ENV="dev"
elif [ $1 == "uat" ]; then
  ENV="uat"
elif [ $1 == "prod" ]; then
  ENV="prod"
else
  ENV="local"
fi

cp env/$ENV.env .env
echo $ENV