from django.test import TestCase
from django.urls import reverse


class IndexPageTest(TestCase):
    def test_fields_visible(self):
        """
        If no questions exist, an appropriate message is displayed.
        """
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Zip Code")
        self.assertContains(response, "Longitude")
        self.assertContains(response, "Latitude")

