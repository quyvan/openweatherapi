$(document).ready(function () {
    $("form[name=openweather_form]").submit(function (event) {
        event.preventDefault();
        const formData = {
            city: $("#city").val(),
            zip_code: $("#zip_code").val(),
            latitude: $("#latitude").val(),
            longitude: $("#longitude").val(),
            csrfmiddlewaretoken: getCookie('csrftoken')
        };
        if (isValidOpenWeatherForm(formData)) {
            $('#errorMessage').attr('hidden', true);
            $.ajax({
                type: "POST",
                url: "/openweather/api/query",
                data: formData,
                dataType: "json",
                encode: true,
            }).done(function (data) {
                $('#results').html(JSON.stringify(data));
            });
        } else {
            $('#errorMessage').attr('hidden', false);
        }
    });
});

const getCookie = (name) => {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

const isValidOpenWeatherForm = (formData) => {
    /*
    1) one field must be entered
    2) zip code should be 5 characters in length, all numbers
    3) latitude and longitude is a set
     */
    if (!formData.city && !formData.zip_code && !formData.longitude && !formData.latitude) {
        return false;
    } else if (!!formData.zip_code && formData.zip_code.length !== 5) {
        return false;
    } else if (!!formData.longitude && !formData.latitude) {
        return false;
    } else if (!!formData.latitude && !formData.longitude) {
        return false;
    }
    return true;
};