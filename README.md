# README #

### What is this repository for? ###
- Demonstrate openweather api queries

### How do I get set up? ###
- install python
- install venv
- activate virtualenv - `source activate venv/bin/activate`
- to setup local env - `bash setup.sh local`
- to run server - `python manage.py runserver`
- to test - `python manage.py test`

### Files for reviewer to take note of ###
- /emersonapp/static/js/scripts.js
- /emersonapp/templates/emersonapp/general/index.html
- /openweather/views.py
- /openweather/OpenWeatherApi.py
- /openweather/tests.py

### 4+1 View ###
#### Scenarios: ####
* user can search current weather conditions using openweathermap.org/api by:
  * city name - currently disabled in ui due to time constraints and additional complexities with state and  country codes
  * zip code (assuming zip code only, as opposed to postal code)
  * coordinates 

#### Logical View ####
[Diagram](https://drive.google.com/file/d/1I8ASh7pdQ1LoWA8x8oSujCF5JjTqteRe/view?usp=sharing)

#### Process View ####
##### Security #####
* we want to keep the api key for openweather api hidden
* form submit should have csrf to deter attacks
##### Notes #####
* If the api key is not a security issue, it may make more sense to submit the data from the page to the open weather api directly. This also allows us to avoid worrying about performance concerns. It may also allow us to reduce cost for a sever, and we can use AWS Lambda instead to create a serverless function to hit openweather api.
* No other known developer or user quality to be concerned about at this time.
#### Development View ####
* Using Django as it has many out of the box functionalities that allow us to focus on the functional requirements of the task
Source code will be stored on bitbucket

#### Physical View ####
[Diagram](https://drive.google.com/file/d/19GzQa4O0OlHa9yU0p46Y3v21vmmjkmlM/view?usp=sharing)