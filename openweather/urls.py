from django.urls import path

from . import views

app_name = 'openweather'
urlpatterns = [
    path('api/query', views.openweather_query, name='openweather_query'),
]
