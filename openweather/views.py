from django.http import JsonResponse, HttpResponse
from django.shortcuts import render


# Create your views here.
from openweather.OpenWeatherApi import OpenWeatherApi


def openweather_query(request):
    # Todo ensure data is cleaned
    city = request.POST.get('city')
    zip_code = request.POST.get('zip_code')
    latitude = request.POST.get('latitude')
    longitude = request.POST.get('longitude')

    if not city and not zip_code and not latitude and not longitude:
        return JsonResponse({'error': 'No field entered'})

    if len(zip_code) not in (5, 0):
        return JsonResponse({'error': 'Zip Code Invalid'})

    if (latitude and not longitude) or (longitude and not latitude):
        return JsonResponse({'error': 'Latitude and longitude invalid'})

    weather_api = OpenWeatherApi()
    try:
        if latitude and longitude:
            result = weather_api.get_weather_from_coordinates(latitude, longitude)
            return JsonResponse(result)
        elif zip_code:
            latitude, longitude = weather_api.get_coordinates_from_zip_code(zip_code)
            result = weather_api.get_weather_from_coordinates(latitude, longitude)
            return JsonResponse(result)
        elif city:
            latitude, longitude = weather_api.get_coordinates_from_city(city)
            result = weather_api.get_weather_from_coordinates(latitude, longitude)
            return JsonResponse(result)
    except Exception as e:
        return HttpResponse(status=400)
    return HttpResponse(status=400)
