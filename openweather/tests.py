from django.test import TestCase

from openweather.OpenWeatherApi import OpenWeatherApi


class OpenWeatherAPITest(TestCase):
    def setUp(self):
        self.weather_api = OpenWeatherApi()

    def test_zip_code(self):
        """
        If no questions exist, an appropriate message is displayed.
        """
        zip_code = '12345'
        lat, lon = self.weather_api.get_coordinates_from_zip_code(zip_code)
        json_resp = self.weather_api.get_weather_from_coordinates(lat, lon)
        self.assertIsNotNone(json_resp, 'No response returned')
