import requests
from django.conf import settings


class OpenWeatherApi:
    def __init__(self):
        # self.app_id = ''
        self.app_id = settings.OPEN_WEATHER_API_APP_ID
        self.root_url = settings.OPEN_WEATHER_API_URL

    # Only grabbing first city to reduce complexity and scope
    def get_coordinates_from_city(self, city):
        url = self.root_url + 'geo/1.0/direct?q=' + city + '&limit=5&appid=' + self.app_id
        response = requests.get(url)
        result = response.json()
        return result[0].get('lat'), result[0].get('lon')

    def get_coordinates_from_zip_code(self, zip_code):
        url = self.root_url + 'geo/1.0/zip?zip=' + zip_code + '&appid=' + self.app_id
        response = requests.get(url)
        result = response.json()
        return result.get('lat'), result.get('lon')

    def get_weather_from_coordinates(self, lat, long):
        url = self.root_url + 'data/2.5/weather?lat=' + str(lat) + '&lon=' + str(long) + '&appid=' + self.app_id
        response = requests.get(url)
        result = response.json()
        return result
